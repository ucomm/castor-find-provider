a demo module that uses a custom composer loader to direct castor modules to the correct install path.

# Find A Provider Module

The main thing to keep in mind when working on this module is that the dev and prod API endpoints can bring back very different information. For instance, some providers on the dev endpoint will have images attached whereas on the prod endpoint they don't. When you're working, that means some images may appear to be broken. However, providers most likely won't show up on production health server anyway. 

Another issue is that some providers are not meant to be publicly visible. There are flags on the API response (e.g. `IsVisibleInPhysicianDirectory`). In development, those flags don't necessarily match the ones in production leading to potentially more confusion. See [`frontend.php lines 19-26`](includes/frontend.php) for how they get filtered.