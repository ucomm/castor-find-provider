<?php

class FindAProviderModule extends Castor_Module {

    private $version = '1.0.0';

    public function __construct() {
        $apiUrl;
        if ( $_SERVER['HTTP_HOST'] === "health.uconn.edu" ) {
            $apiUrl = 'https://publicdirectoryapi.uchc.edu/api/';
        } else {
            $apiUrl = 'https://xat-home.uchc.edu/PublicDirectory/api/';
        }
        if ( !defined( 'FIND_PROVIDER_API_URL' ) ) {
            define( 'FIND_PROVIDER_API_URL', $apiUrl );
        }
    }

    public function load_module() {
        if ( class_exists( 'FLBuilder' ) ) {
            require_once 'find-a-provider.php';
            $specs = $this->get_provider_specialties();
            $dspecs = $this->get_provider_dental_specialties();
            $locations = $this->fetch_locations();
            $spec_select_options = array();
            $dspec_select_options = array();
            for ( $i = 0; $i < count($dspecs); $i++ ) {
                if ( $dspecs[$i]->IsVisible ) {
                    $dspec_select_options[$dspecs[$i]->Name] = __( $dspecs[$i]->Name, 'castor' );
                }
            }
            for ( $i = 0; $i < count($specs); $i++ ) {
                if ( $specs[$i]->IsVisible ) {
                    $spec_select_options[$specs[$i]->Name] = __( $specs[$i]->Name, 'castor' );
                }
            }
            FLBuilder::register_module('FindAProvider', array(
                'specialties' => array(
                    'title' => __( 'Specialties', 'castor' ),
                    'sections' => array(
                        'specs' => array(
                            'title' => __( 'Specialties', 'castor' ),
                            'fields' => array(
                                'specialty_conditional' => array(
                                    'type' => 'select',
                                    'label' => __( 'Choose Specialty Type', 'castor' ),
                                    'options' => array(
                                        'non-dental' => __( 'Non Dental', 'castor' ),
                                        'dental' => __( 'Dental', 'castor' )
                                    ),
                                    'default' => 'non-dental'
                                ),
                                'specialties_field' => array(
                                    'type' => 'select',
                                    'label' => __( 'Choose Specialties', 'castor' ),
                                    'options' => $spec_select_options,
                                    'multi-select' => true
                                ),
                                'dental_specialties_field' => array(
                                    'type' => 'select',
                                    'label' => __( 'Choose Dental Specialties', 'castor' ),
                                    'options' => $dspec_select_options,
                                    'multi-select' => true
                                ),
                                'locations_field' => array(
                                    'type' => 'select',
                                    'label' => __( 'Choose Locations (Optional)', 'castor' ),
                                    'options' => $locations,
                                    'multi-select' => true
                                ),
                                'sort_order' => array(
                                    'type' => 'select',
                                    'label' => __( 'Sort Order', 'castor' ),
                                    'options' => array(
                                        'lastname_asc' => __( 'Last Name ASC', 'castor' ),
                                        'lastname_desc' => __( 'Last Name DESC', 'castor' ),
                                        'accepting_patients' => __( 'Accepting Patients', 'castor' ),
                                    ),
                                    'default' => 'asc'
                                ),
							)
						),
						'specialty_heading' => array(
							'title' => 'Heading',
							'fields' => array(
                                'specialty_heading_text' => array(
                                    'type' => 'text',
                                    'label' => __( 'Enter Heading Text', 'castor' )
                                ),
                                'specialty_heading_hide' => array(
                                    'type' => 'select',
                                    'label' => __( 'Hide Heading', 'castor' ),
									'options' => array(
										'Yes' => 'Yes',
										'No' => 'No'
									),
									'default' => 'No'
                                )
                            )
                        ),
                        'display_options' => array(
                            'title' => 'Display Options',
                            'fields' => array(
                                'hide_locations' => array(
                                    'type' => 'select',
                                    'label' => __( 'Hide Locations', 'castor' ),
                                    'options' => array(
                                        0 => __('Show', 'castor'),
                                        1 => __('Hide', 'castor')
                                    )
                                ),
                                'hide_patient_status' => array(
                                    'type' => 'select',
                                    'label' => __( 'Hide Patient Status', 'castor' ),
                                    'options' => array(
                                        0 => __('Show', 'castor'),
                                        1 => __('Hide', 'castor')
                                    )
                                ),
                                'hide_bio_button' => array(
                                    'type' => 'select',
                                    'label' => __( 'Hide Bio Button', 'castor' ),
                                    'options' => array(
                                        0 => __('Show', 'castor'),
                                        1 => __('Hide', 'castor')
                                    )
                                ),
                                'hide_phone_number' => array(
                                    'type' => 'select',
                                    'label' => __( 'Hide Phone Number', 'castor' ),
                                    'options' => array(
                                        0 => __('Show', 'castor'),
                                        1 => __('Hide', 'castor')
                                    )
                                ),
                                'hide_request_appointment' => array(
                                    'type' => 'select',
                                    'label' => __( 'Hide Request Appointment', 'castor' ),
                                    'options' => array(
                                        0 => __('Show', 'castor'),
                                        1 => __('Hide', 'castor')
                                    )
                                )
                            )
                        ),
                        'custom_text' => array(
                            'title' => __('Custom Text', 'castor'),
                            'fields' => array(
                                'custom_texts' => array(
                                    'type' => 'form',
                                    'label' => __('Custom Text', 'castor'),
                                    'form' => 'find_provider_custom_fields_form',
                                    'preview_text' => 'custom_text',
                                    'multiple' => true
                                )
                            )
                        )            
                    )
                )
            ));

            // Repeater field for "Custom Fields"
            FLBuilder::register_settings_form('find_provider_custom_fields_form', array(
                'title' => __('Custom Fields', 'castor'),
                'tabs' => array(
                    'tab_1' => array(
                        'title' => __('Custom Field', 'castor'),
                        'sections' => array(
                            'section_1' => array(
                                'title' => __('General', 'castor'),
                                'fields' => array(
                                    'provider_id' => array(
                                        'type' => 'text',
                                        'label' => __('Provider ID', 'castor'),
                                        'placeholder' => __('Tucker-Joseph', 'castor'),
                                        'description' => __('The Profile ID is the identifier for the provider.  Usually LastName-FirstName.  You can find this by clicking on the "View Bio" button, and copying the last part of the URL.', 'castor'),
                                    ),
                                    'custom_text' => array(
                                        'type' => 'text',
                                        'label' => __('Custom Text', 'castor'),
                                        'placeholder' => __('Director of Department', 'castor')
                                    ),
                                    'show_after' => array(
                                        'type' => 'select',
                                        'label' => __('Display After', 'castor'),
                                        'default' => '',
                                        'options' => array(
                                            'name' => __('Name', 'castor'),
                                            'locations' => __('Locations', 'castor'),
                                            'patient_status' => __('Patient Status', 'castor'),
                                            'bio_button' => __('Bio Button', 'castor'),
                                            'phone_number' => __('Phone Number', 'castor'),
                                            'request_appointment' => __('Request Appointment', 'castor')
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            ));
        }
    }

    /**
     * Retrieve list of specialties from the provider API.
     */
    private function get_provider_specialties() {
        $service = new UConnHealth\SpecialtyCollection();
        $service->setDoctor();
        $service->enableWpCache();
        $service->getSpecialties();
        $service->sortByName();
        return $service->specialties;
    }


    /**
     * Retrieve list of dental specialties from the provider API.
     */
    private function get_provider_dental_specialties() {
        $service = new UConnHealth\SpecialtyCollection();
        $service->setDentist();
        $service->enableWpCache();
        $service->getSpecialties();
        $service->sortByName();
        return $service->specialties;
    }

    /**
     * Returns all locations
     * @return array    Location Objects
     */
    private function fetch_locations() {
        return array(
            'Avon'          => 'Avon',
            'Canton'        => 'Canton',
            'East Hartford' => 'East Hartford',
            'Farmington'    => 'Farmington',
            'Hartford'      => 'Hartford',
            'Simsbury'      => 'Simsbury',
            'Southington'   => 'Southington',
            'Storrs'        => 'Storrs',
            'West Hartford' => 'West Hartford'
        );
    }

}