<?php

class FindAProvider extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Find a Provider', 'castor' ),
            'description'     => __( 'Pull in physicians/dentists with a nice layout.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/find-a-provider/',
            'url'             => CASTOR_URL . 'modules/find-a-provider/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }

    /**
     * Fetches dental or non-dental physicians from CFAR.
     * @param  array                            $specs Specialty name strings.
     * @param  string                           $type  "DOCTOR" or "DENSTIST" to specify which providers.
     * @return UConnHealth\ProviderCollection   A Provider collection object.
     */
    public function fetch_providers( $specs = array(), $type = "doctor" ) {
        $service = new UConnHealth\ProviderCollection();
        if ( strtoupper($type) === "DENTIST" ) {
            $service->setDentist();
        } else {
            $service->setDoctor();
        }
        $service->enableWpCache();
        $service->addSpecialties($specs);
        $service->fetchProviders();
        return $service;
    }

    /**
     * Organize a list of providers by the requested specialties.
     * @param  UConnHealth\ProviderCollection  $providers List of provider objects from CFAR.
     * @param  array  $specs     List of specialty names that should delegate provider groups.
     * @param  array  $locations List of locations that are being filtered by.
     * @return array             Associative array of specialty names to arrays of providers with those specs.
     */
    public function organize_providers_by_chosen_specs( $providers = null, $specs = array(), $sort = 'lastname_asc', $locations = array() ) {
        if ( !$providers ) {
            return array();
        }
        $filter_by_location = !empty($locations);

        if ( $filter_by_location ) {
            $providers->filterByLocation( $locations );
        }

        switch ($sort) {
            case("lastname_asc"):
                $providers->sortByLastName('asc');
                break;
            case("lastname_desc"):
                $providers->sortByLastName('desc');
                break;
            case("accepting_patients"):
                $providers->sortByAcceptingPatients('asc');
                break;
        }

        return $providers->groupBySpecialty( $specs );
    }

    /**
     * Assign custom texts to providers if they exist.
     * @param  UConnHealth\ProviderCollection    $provider_collection      Providers as given by CFAR.
     * @param  array    $custom_texts   Array of custom texts provided in the module.
     * @return UConnHealth\ProviderCollection    Returns the provider collection as given by CFAR, with an added custom texts property.
     */
    public function assign_custom_texts( $provider_collection, $custom_texts ) {
        foreach($provider_collection->providers as $index => $prov) {
            $provider_collection->providers[$index]->custom_text = array();
            foreach($custom_texts as $ct) {
                if($ct->provider_id === $prov->ProfileId) {
                    array_push( $provider_collection->providers[$index]->custom_text, $ct );
                }
            }
        }
        return $provider_collection;
    }

    /**
     * Check for a custom text property on a provider, and output if at the proper location.
     * @param  object   $provider       A provider object.
     * @param  string   $show_after     A text key delegating the location of the custom text.
     * @return string   Outputs the custom text to the page if necessary, otherwise no output.
     */
    public function custom_text( $provider, $show_after = 'name' ) {
        foreach($provider->custom_text as $ct) {
            if($ct->show_after === $show_after) {
                echo '<p class="health-find-provider-custom-text">' . $ct->custom_text . '</p>';
            }
        }
    }
}