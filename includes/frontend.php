<div class="fl-find-provider health-find-provider">
    <?php
    $specs;
    $type;
    $providers_by_spec;
    if ( $settings->specialty_conditional === 'non-dental' ) {
        $specs = $settings->specialties_field;
        $type = "DOCTOR";
    } else {
        $specs = $settings->dental_specialties_field;
        $type = "DENTIST";
    }
    $sort_order = $settings->sort_order;
    $providers = $module->fetch_providers( $specs, $type );
    $providers = $module->assign_custom_texts( $providers, $settings->custom_texts );
    $organized = $module->organize_providers_by_chosen_specs( $providers, $specs, $sort_order, $settings->locations_field );

    // only allow publicly visible providers
    $organized = array_filter($organized, function($prov) {
        $visible_dr = $prov[0]->IsVisibleInPhysicianDirectory;
        $visible_dentist = $prov[0]->IsVisibleInDentistDirectory;

        if ($visible_dr || $visible_dentist) {
            return $prov;
        }
    });

    unset($providers);


    if ( !empty($organized) ) {
        foreach ( $organized as $sname => $provider ) {
        ?>
            <div class="health-find-provider-specsection">
<?php if ($settings->specialty_heading_hide == 'No'): ?>
                <h3><?php echo (!empty($settings->specialty_heading_text)) ? $settings->specialty_heading_text : $sname; ?></h3>
<?php endif; ?>
                <div class="row">
                    <?php
                    foreach( $provider as $prov ) {
                    ?>
                    <div class="col-xs-12 col-sm-6 health-find-provider-single">
                        <div class="row">
                            <div class="col-xs-8 col-sm-4">
                                <img src="<?php echo $prov->getImageSource(CASTOR_URL . "modules/find-a-provider/assets/img/oak-leaf.jpg"); ?>" alt="<?php echo $prov->getFullName(); ?>" class="img-responsive thumbnail health-find-provider-thumb">
                            </div>
                            <div class="col-xs-12 col-sm-8 health-find-provider-data">
                                <p class="health-find-provider-name"><strong><?php echo $prov->getFullName(); ?></strong></p>
                                <?php $module->custom_text($prov, 'name'); ?>
                                <?php if ( !$settings->hide_locations ) { ?><p class="health-find-provider-location"><?php echo $prov->getLocationsString(); ?></p><?php } ?>
                                <?php $module->custom_text($prov, 'locations'); ?>
  								<?php if ( !$settings->hide_patient_status ) { ?><p class="health-find-provider-accepting-patients<?php echo ($prov->IsAcceptingPatients == 1) ? ' accepting' : ' not-accepting'; ?>"><?php echo ($prov->IsAcceptingPatients == 1) ? 'Accepting New Patients' : 'Not Accepting New Patients'; ?></p><?php } ?>
                                <?php $module->custom_text($prov, 'patient_status'); ?>
                                <?php if ( !$settings->hide_bio_button ) { ?><p class="health-find-provider-bio"><a href="http://health.uconn.edu/find-a-provider/physician/<?php echo $prov->ProfileId; ?>" class="btn btn-health" aria-label="biography for <?php echo $prov->getFullName(); ?>">View Bio</a></p><?php } ?>
                              	<?php $module->custom_text($prov, 'bio_button'); ?>
                                <?php if ( !$settings->hide_phone_number ) { ?><p class="health-find-provider-phone"><a href="tel:18443882666">1-84-GET-UCONN</a></p><?php } ?>
                                <?php $module->custom_text($prov, 'phone_number'); ?>
                                <?php if ( !$settings->hide_request_appointment ) { ?><p class="health-find-provider-req"><a href="http://health.uconn.edu/find-a-provider/physician/<?php echo $prov->ProfileId; ?>" aria-label="request an appointment with <?php echo $prov->getFullName(); ?>">Request an Appointment</a></p><?php } ?>
                                <?php $module->custom_text($prov, 'request_appointment'); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        <?php
        }
    } else {
        if ( is_user_logged_in() ) {
            _e( '<p>No valid specialties selected.</p>', 'uconn-health-clinical' );
            if ( current_user_can('edit_pages') ) {
                _e( '<p>It is possible if you chose a valid speciality, that there are no providers available.  These specialties can be selected in the beaver builder "Find a Provider" module.  Keep in mind that if your selected "Specialty Type" is "Non Dental", then you must select at least one non-dental specialty.</p>', 'uconn-health-clinical' );
            }
        }
    }
    ?>
    
</div>